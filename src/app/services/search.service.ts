import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Match } from '../models/match';
import { SearchParams } from '../models/search_params';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private serverURL: string = "https://wurub-web-scraping.herokuapp.com/search"

  constructor(private http:HttpClient) { }

  doSearch(searchParams: SearchParams): Observable<Match[]> {
    return this.http.post(this.serverURL, searchParams).pipe(
      map(response => response as Match[])
    );
  }
}
