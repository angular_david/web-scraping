import { Component, OnInit } from '@angular/core';
import { Match } from 'src/app/models/match';
import { SearchParams } from 'src/app/models/search_params';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  matches: Match[]
  keywords: string
  searching: boolean

  constructor(private service: SearchService) { }

  ngOnInit(): void {
    this.matches = []
    this.keywords = ''
    this.searching = false
  }

  doSearch() {
    const searchParams: SearchParams = {
      keywords: this.keywords,
      filter_title: true
    }
    this.service.doSearch(searchParams).subscribe(
      response => {
        this.matches = response
        this.searching = true
      }
    );
  }

}
