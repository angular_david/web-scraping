export class SearchParams {
    keywords: string
    filter_title?: boolean
    filters_price?: FilterPrice[]
    sources?: string[]
}

export class FilterPrice {
    min?: number
    max?: number
}